Traduction de l'application [CEDy](https://framagit.org/ced/cedy/1-afficher) (Calculette : Eco-déplacements, écrite en Python)
dans le langage javascript...

Cette version utilise les feuilles de styles pour optimiser la présentation.

Voir la [demo](https://ced.frama.io/CEDjs/1-afficher_evol/) mise
en œuvre via les gitlab pages et l'intégration continue...
